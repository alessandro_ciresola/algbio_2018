%% Sokolov distance vs exact edit distance 
x = [1:1:691]; 
f1 = figure; 
plot(x,SKD) 
hold on; 
plot(x, EXD,'r')
hold off; 
axis([0 300 560 760]);
ylabel('Sokolov distance and exact edit distance'); 
title('Sokolov distance vs exact edit distance'); 
legend('sokolov distance', 'Exact edit distance'); 

%% Upper bound, Lower bound and DELTA 
f2 = figure; 
plot(x,LOW,'r'); 
hold on; 
plot(x,UPP, 'b'); 
plot(x,DEL, 'g');
title('Upper bound, lower bound and delta'); 
legend('Lower bound','Upper bound','Delta','Location','northwest');
axis([1 150 10 150]);

%% Calcolo del valore medio per il Delta 
meanDelta = mean(DEL) 
hold off; 

%% Sokolov time VS exact edit distance time 
f3 = figure; 
SK = SKT(1:300);
EX = EXT(1:300); 
y = (1:1:300);
meanSokolovTime = mean(SKT) 
meanExactTime = mean(EXT) 
radixExactTime = sqrt(meanExactTime) 
plot(y,SK,'r')
hold on; 
plot(y,EX,'b') 
hold off; 
axis([0 150 0 25]);
title('Sokolov time VS exact edit distance time'); 
legend('Sokolov time','Exact edit distance time');
