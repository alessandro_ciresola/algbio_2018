import java.util.Scanner; 

public class LongestCommonSubsequenceAlgBio 
{ 
  public static void main(String[] args) 
  { 
    Scanner in = new Scanner(System.in); 
    System.out.println("Calcolo della Longest Common Subsequence di una coppia di stringhe fornite in ingresso..."); 
    /*
    System.out.println(); 
    System.out.println("Inserisci la prima stringa: "); 
    String xi = in.next(); 
    System.out.println("Inserisci la seconda stringa: "); 
    String yi = in.next(); 
    */
    
    String xi = args[0]; 
    String yi = args[1]; 

    int m = xi.length(); 
    int n = yi.length(); 

    char[] x = new char[m+1]; //xi = 0...........m 
    char[] y = new char[n+1]; //yi= 0............n
    
    for(int i = 0; i < m; i++) 
    { 
      x[i+1] = xi.charAt(i); 
    }
    
    for(int j = 0; j < n; j++) 
    { 
      y[j+1] = yi.charAt(j); 
    }  

    int[][] commSubLen = LCS(x, y, m, n); 
    int commLength = commSubLen[m][n]; 
    System.out.println("La lunghezza della Longest Common Subsequence è pari a : " + commLength );
    int editDistance = n + m - 2*(commLength);
    System.out.println("La edit distance delle due stringhe fornite in input e' quindi pari a: " + editDistance);  

    //char[][] results = LCSL(x, y, m,  n , commSubLen);  
    
    //PARTE PER LA STAMPA DELLA LONGEST COMMON SUBSEQUENCE 
    /*************************
    System.out.println(); 
    System.out.println("La Longest Common Subsequence delle due stringhe digitate in input è pari a : "); 
      Print_LCS( x, m, n, results ); 
    System.out.println();
    *************************/ 
  }//[m]main

//Per la stampa della Longest Common Subsequence 
public static void Print_LCS(char[] x, int i, int j, char[][] results) 
{
  if(i == 0) return; 
  if(j==0) return;   

  if( results[i][j] == 'd' ) 
  { 
     Print_LCS(x, i-1, j-1, results); 
     System.out.print(x[i]); 
  }
  else 
  { 
       if(results[i][j] == 'l') 
          Print_LCS(x, i, j-1, results);
       else 
          Print_LCS(x, i-1, j, results);
  } 
  return; 
}//[m]Print_LCS

//Per il calcolo della lunghezza della LCS 
public static int[][] LCS(char[] x, char[] y, int m, int n) 
{ 
  int[][] L = new int[m+1][n+1];
  int i; 
  int j; 

  for(i=0; i<m+1; i++) 
     L[i][0] = 0; 
   
  for(j=1; j<n+1; j++) 
     L[0][j] = 0; 

  for(i=1; i<m+1; i++) 
   { 
     for(j=1; j<n+1; j++) 
      { 
        if(x[i]==y[j]) 
         { 
           L[i][j] = L[i-1][j-1] + 1; 
         } 
        else 
        { 
          if( L[i-1][j] >= L[i][j-1] ) 
           { 
             L[i][j] = L[i-1][j] ; 
           } 
          else 
           { 
             L[i][j] = L[i][j-1] ; 
           } 
         } 
      } 
   } 
  return L; 
}//[m]LCS

//Per il calcolo della Longest Common Subsequence 
public static char[][] LCSL(char[] x, char[] y, int m, int n , int[][] L) 
{ 
  char[][] b = new char[m+1][n+1];
  int i; 
  int j; 

  for(i=1; i<m+1; i++) 
  { 
     for(j=1; j<n+1; j++) 
      { 
        if(x[i]==y[j]) 
         { 
           b[i][j] = 'd' ;   // procedi in diagonale nella tabella...   
          
         } 
        else 
        { 
          if( L[i-1][j] >= L[i][j-1] ) 
           { 
             b[i][j] = 'u'; //up vai in su nella tabella... 
           } 
          else 
           { 
             b[i][j] = 'l' ; // left vai a sx nella tabella... 
           } 
         } 
      } 
  } 
  return b ; 
}//[m]LCSL 

}//{c}LongestCommonSubsequenceAlgBio
