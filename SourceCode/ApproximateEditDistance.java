import java.util.*; 

public class ApproximateEditDistance 
{
	public static final int ALPHABET_SIZE = 4; 

	public static void main(String[] args) 
	{
	  //String s1 = "atcgtctgaccg"; 
	  //String s1 = args[0]; 
	  //String s2 = "acgttaccagtc"; 
      //String s2 = args[1]; 
      int w = Integer.parseInt(args[0]); 
      int q1 = Integer.parseInt(args[1]);
      int q2 = Integer.parseInt(args[2]);
      
      Scanner sc = new Scanner(System.in); 

      ArrayList<String> dnaSequences = new ArrayList<String>(); 

      while(sc.hasNextLine())
      {
      	dnaSequences.add(sc.nextLine()); 
      }

      String s1 = null; 
      String s2 = null; 

      for(int i = 0; i < dnaSequences.size()-1; i++)
      {
      	for(int j = (i+1); j < dnaSequences.size(); j++)
      	{
           s1 = dnaSequences.get(i); 
           System.out.println("Dimensione stringa s1: " + s1.length());  
           s2 = dnaSequences.get(j); 
           System.out.println("Dimensione stringa s2: " + s2.length());  
           long oldTime = System.currentTimeMillis(); 
           long distance = editDistance(s1, s2, w, q1, q2);
           long newTime = System.currentTimeMillis();  
           long computingTimeRequired = newTime - oldTime; 

           System.out.println(distance + " " + computingTimeRequired);
      	}
      }

	  //int res = qGramDistance(s1, s2, 3); 
      /*
      long oldTime = System.currentTimeMillis(); 
      long distance = editDistance(s1, s2, w, q1, q2);
      long newTime = System.currentTimeMillis();  
      long computingTimeRequired = newTime - oldTime; 
      */

      /*
      System.out.println("La edit distance approssimata tra le stringhe fornite in input e' pari a: " + distance); 
      System.out.println("Tempo trascorso: " + computingTimeRequired + " ms");
      */ 

      //System.out.println(distance + " " + computingTimeRequired);

	}//[m]main 

    public static long editDistance(String x, String y, int w, int q1, int q2)
    {
       int n = x.length(); 
       int m = y.length(); 

       if(n != m)
       {
       	 System.out.println("L'approssimazione della Edit Distance non e' calcolabile in quanto le stringhe fornite in input hanno lunghezza differente!"); 
       	 System.exit(1); 
       }

       //Denominatore della formula (5) del paper di Sokolov 
       int den = (n - w + 1)*(q2 - q1 + 1); 
       double totalSum = 0; 

       for(int j = 0; j <= n-w; j++)
       {
         totalSum = totalSum + epsDistance(x.substring(j,j+w), y.substring(j,j+w), q1, q2);  
       }

       double approximateDistance = totalSum / den;
       long roundedResult = Math.round(approximateDistance); 

       return roundedResult; 
    }//[m]editDistance

    public static double epsDistance(String x, String y, int q1, int q2)
    {
       double partialSum = 0; 

       for(int q = q1; q <= q2; q++)
       {
       	 partialSum = partialSum + qGramDistance(x, y, q); 
       }

       return partialSum; 
    }//[m]epsDistance

    //**********************************************************************************
    //Complessità del calcolo della q-gram distance tra due stringhe 
    // se |x|=|y|=n
    //Calcola la dq(x,y) in tempo O(n + m + ALPHABET^q) = O(n + ALPHABET^q) = O(n + 4^q) 
	//**********************************************************************************
	public static int qGramDistance(String x, String y, int q)
	{
       if(x.length() != y.length())
       {
       	System.out.println("Errore: Le stringhe fornite in input non sono della stessa lunghezza!"); 
       	return -1; 
       } 

       int numbComb = (int)(Math.pow((double)ALPHABET_SIZE, (double)q)); 
       int[] resVec = new int[numbComb];  
       
       //Inizializzo il vettore contenente tutte le possibili combinazioni con tutti zeri 
       for(int i = 0; i < resVec.length; i++)
       {
       	 resVec[i] = 0; 
       }  

       //Considero le sottostringhe di x e di y del tipo x[i:i+q-1] e y[i:i+q-1] con i=0...n-q = 0...(x.length()-q)
       int prevRank = 0; //Mantengo il rank del q-gramma precedente calcolato 
       char prevChar = 0; 
       for(int i = 0; i <= x.length()-q; i++)
       {
       	  String q_gram = x.substring(i,i+q); 
          int rk;  

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);    
            resVec[rk]++; 
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
            resVec[rk]++; 
          }
        
          prevRank = rk; 
          prevChar = q_gram.charAt(0);
       }

       /*********************************************************************************
       //***********Debugging********************(Non contare nella complessità)
       System.out.println("Valori contenuti nell'array resVec:");
       for(int j = 0; j < resVec.length; j++)
       {
       	 System.out.println(resVec[j]);
       }
       **********************************************************************************/

       prevRank = 0; 
       prevChar = 0; 
       for(int i = 0; i <= y.length()-q; i++)
       {
       	  String q_gram = y.substring(i,i+q); 
          int rk; 

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);    
            resVec[rk]--; 
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
            resVec[rk]--; 
          }

          prevRank = rk; 
          prevChar = q_gram.charAt(0);
       }

       //Eseguendo la somma di tutte le componenti del vettore resVec in modulo si ottiene la q-gram distance tra le due stringhe x e y 
       int sumResVec = 0; 
       for(int i = 0; i < resVec.length; i++)
       {
       	 sumResVec = sumResVec + Math.abs(resVec[i]); 
       }

       return sumResVec; 
	}//[m]qGramDistance

	public static int rank(String s)
	{
	  int q_gram_size = s.length(); 
	  int sum = 0; 

	  for(int i = 0; i < q_gram_size; i++)
	  {
        sum = sum + (f(s.charAt(i)) * (int)(Math.pow((double)ALPHABET_SIZE, (double)(q_gram_size-1-i))));  
	  }

      return sum; 
	}//[m]rank

	public static int f(char ch)
	{
       char chL = Character.toLowerCase(ch);  
       int res = -1; 

       switch(chL)
       {
       	case 'a': 
       	  res = 0; 
       	  break; 
       	case 'c': 
       	  res = 1; 
       	  break; 
       	case 'g':
       	  res = 2; 
       	  break; 
       	case 't': 
       	  res = 3; 
       	  break; 
       	default: 
       	  System.out.println("Err: carattere non codificabile!");
       	  System.exit(0); 
       }
       return res; 
	}//[m]f
}//{c}AlgBioProject
