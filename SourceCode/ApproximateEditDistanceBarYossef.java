import java.util.*; 
import java.io.*;

/**
*Edit distance approssimata sfruttando il metodo di Bar-Yosseff 
*@version 1.0
*/

public class ApproximateEditDistanceBarYossef 
{
	private static final int ALPHABET_SIZE = 4;  

	public static void main(String[] args) 
	{
		//Parametro k richiesto in input dal metodo di Bar-Yossef 
		// Occhio! 0 <= k < sqrt(n)
		int k = Integer.parseInt(args[0]); 
        //Per ricevere la stringhe in input 
        Scanner sc = new Scanner(System.in); 
        ArrayList<String> dnaSequences = new ArrayList<String>(); 
        PrintWriter out = null; 
        
        try
        {
          //args[1] contiene il nome del file su cui si desidera scrivere l'output 
          out = new PrintWriter(args[1]);
        }
        catch(FileNotFoundException fnfe)
        {
          fnfe.printStackTrace(); 
        }
        
        while(sc.hasNextLine())
        {
      	  dnaSequences.add(sc.nextLine()); 
        }
        
        int n = dnaSequences.get(0).length(); 
        
        //Controllo se il parametro k fornito in input si trova nel range corretto 
        //ovvero se 0 <= k < sqrt(n)     
        if((k < 0) || (k >= Math.sqrt((double)n)))
        {
          System.out.println("Il parametro k fornito in input non rientra nel range corretto!!"); 
          System.exit(1); 
        }

        //Calcolo il q in base al k che e' stato fornito in ingresso e in base alla lunghezza delle stringhe che si hanno in input 
        // q = (n^(2/3))/(2k^(1/3)) 
        double qq = (Math.pow((double)n,(((double)2)/3)))/(2*(Math.pow((double)k,((double)1)/3))); 
        System.out.println("Valore di qq pari a : " + qq); 
        int q = (int)Math.round(qq); 

        System.out.println("Valore di q pari a : " + q); 

        String s1 = null; 
        String s2 = null; 

        out.println("B&YD " + "B&YTime " + "EXD " + "EXT " + "DEL " + "UPPB"); 

        for(int i = 0; i < dnaSequences.size()-1; i++)
        {
      	   for(int j = (i+1); j < dnaSequences.size(); j++)
      	   {
            s1 = dnaSequences.get(i); 
            s2 = dnaSequences.get(j); 
            long initTime = System.nanoTime();
            long barYossefDistance = approximateBarYossefDistance(s1, s2, q); 
            long finishTime = System.nanoTime(); 
            long diffTime = finishTime-initTime; 
            
            //Calcolo della exact edit distance 
            String xi = s1; 
            String yi = s2;  
        
            int m = xi.length(); 
            n = yi.length();
            
            char[] x = new char[m+1]; //xi = 0...........m 
            char[] y = new char[n+1]; //yi= 0............n

            for(int h = 0; h < m; h++) 
            {  
              x[h+1] = xi.charAt(h); 
            }
    
            for(int kk = 0; kk < n; kk++) 
            {  
              y[kk+1] = yi.charAt(kk); 
            } 

            long oldTime2 = System.nanoTime(); 
            int[][] commSubLen = LCS(x, y, m, n); 
            int commLength = commSubLen[m][n]; 
            // ED(x,y) = |x|+|y|-2*|LCS(x,y)| 
            int editDistance = n + m - 2*(commLength);
            long newTime2 = System.nanoTime(); 
            long timeRequired = newTime2 - oldTime2;

            //Calcolo dell'upper bound 
            long barYossefUpperBound = (long)(2*(Math.pow((double)(editDistance*n),((double)2/3)))); 
            out.println(barYossefDistance + " " + diffTime + " " + editDistance + " " + timeRequired + " " + Math.abs(editDistance - barYossefDistance) + " " + barYossefUpperBound); 
           }//endfor
        }//endfor
        out.flush(); 
	}//{c}main

    public static long approximateBarYossefDistance(String x, String y, int q)
    {
       int n = x.length(); 
       int m = y.length(); 

       if(n != m)
       {
       	 System.out.println("La edit distance approssimata di Bar-Yossef non e' calcolabile in quanto le stringhe sono di lunghezza diversa!"); 
       	 System.exit(1); 
       }
       
       //Calcolo l'ultimo indice di blocco che puo' essere ottenuto da x 
       int k = (n-q); 
       int maxIndex = (int)(Math.floor(((double)((k+1)*q))/n)); 
       System.out.println("Valore di maxIndex pari a :" + maxIndex); 
       /*
       if(maxIndex != (q-1))
       {
       	  System.out.println("Errore: l'indice atteso e quello reale non corrispondono!!"); 
       	  System.exit(1); 
       }
       */
       
       // maxIndex + 1 coincide con il numero totale di blocchi 
       double param = (Math.pow((double)ALPHABET_SIZE, (double)q));
       System.out.println("double param pari a : " + param); 
       long paramNew1 = (long)param; 
       int paramNew = 4; 
       int hj = 1024*1024; 
       System.out.println("Math pow Testing : " + Math.pow((double)hj , ((double)2)/3)+ ""); //<-- Da correggere! Math.pow fornisce solo un'approssimazione!
       System.out.println("paramNew1 pari a : " + paramNew1); 
       int numIndexes = paramNew * (maxIndex + 1); 
       
       System.out.println("numIndexes pari a : " + numIndexes); 
       int specialCharacter = (n-q+2); 
       int[] resVec = new int[numIndexes]; 

       for(int i = 0; i < resVec.length; i++)
       {
       	  resVec[i] = specialCharacter; 
       } 
       
       //Lista di indici 
       ArrayList<Integer> indexList = new ArrayList<Integer>(); 

       int prevRank = 0; //Mantengo il rank del q-gramma precedente calcolato 
       char prevChar = 0; //Mantengo il carattere più significativo del q-gramma precedentemente calcolato 
       
       //Lavoro sulla prima stringa x in ingresso 
       for(int i = 0; i <= (n-q); i++)
       {
       	  String q_gram = x.substring(i,i+q); 
 
          //Calcolo l'offset: se la coppia e' ((x1 x2 x3), w) allora il rank nel vettore sara' pari a rank(x1 x2 x3) + w*ALPHA^q
          //dove w e' l'indice di blocco calcolato come parte bassa di ((i+1)*q)/n
          int blockIndex = (int)Math.floor(((double)((i+1)*q))/n); 
          int offset = ((int)(Math.pow((double)ALPHABET_SIZE, (double)q))) * blockIndex ;
          int rk;  

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);     
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
          }
           
          int shiftRank = (rk + offset); 
          if(resVec[shiftRank] == specialCharacter) //Non e' mai stato letto e devo inserire l'indice nella lista indexList 
          {
            //Ho trovato un'occorrenza del q-gramma codificato in rk 
            resVec[shiftRank] = 1; 
            indexList.add(Integer.valueOf(shiftRank)); 
          }
          else
          {
            //Il q-gramma era gia' stato letto in precedenza per cui l'indice e' gia' stato salvato nella indexList 
            resVec[shiftRank]++; 
          }

          prevRank = rk;  
          prevChar = q_gram.charAt(0);
       }//endfor


       //Lavoro sulla seconda stringa y in ingresso 
       prevRank = 0; 
       prevChar = 0; 
       for(int i = 0; i <= n-q; i++)
       {
       	  String q_gram = y.substring(i,i+q); 
          int rk; 
          int blockIndex = (int)Math.floor(((double)((i+1)*q))/n); 
          int offset = ((int)(Math.pow((double)ALPHABET_SIZE, (double)q))) * blockIndex ;

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);     
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
          }
          
          int shiftRank = (rk + offset); 

          if(resVec[shiftRank] == specialCharacter) //Non e' mai stato letto e devo inserire l'indice nella lista indexList 
          {
            //Ho trovato un'occorrenza del q-gramma codificato in rk 
            resVec[shiftRank] = -1;  
            indexList.add(Integer.valueOf(shiftRank)); 
          }
          else
          {
            //Il q-gramma era gia' stato letto in precedenza per cui l'indice e' gia' stato salvato nella indexList 
            resVec[shiftRank]--; 
          }

          prevRank = rk; 
          prevChar = q_gram.charAt(0);
       }//endfor

       int sumResVec = 0; 
       
       for(Integer idx : indexList)
       {
         int intIndex = idx.intValue(); 
         sumResVec = sumResVec + Math.abs(resVec[intIndex]); 
       }

       return sumResVec;
    }//[m]approximateBarYossefDistance 


    //Per il calcolo della lunghezza della LCS 
    public static int[][] LCS(char[] x, char[] y, int m, int n) 
    { 
      int[][] L = new int[m+1][n+1];
      int i; 
      int j; 

      for(i=0; i<m+1; i++) 
        L[i][0] = 0; 
   
      for(j=1; j<n+1; j++) 
        L[0][j] = 0; 

      for(i=1; i<m+1; i++) 
      { 
        for(j=1; j<n+1; j++) 
        { 
          if(x[i]==y[j]) 
          { 
            L[i][j] = L[i-1][j-1] + 1; 
          } 
          else 
          { 
            if( L[i-1][j] >= L[i][j-1] ) 
            { 
              L[i][j] = L[i-1][j] ; 
            } 
            else 
            { 
              L[i][j] = L[i][j-1] ; 
            } 
          } 
        } 
      } 
      return L; 
    }//[m]LCS

    public static int rank(String s)
	{
	  int q_gram_size = s.length(); 
	  int sum = 0; 

	  for(int i = 0; i < q_gram_size; i++)
	  {
        sum = sum + (f(s.charAt(i)) * (int)(Math.pow((double)ALPHABET_SIZE, (double)(q_gram_size-1-i))));  
	  }

    return sum; 
	}//[m]rank

	public static int f(char ch)
	{
       char chL = Character.toLowerCase(ch);  
       int res = -1; 

       switch(chL)
       {
       	case 'a': 
       	  res = 0; 
       	  break; 
       	case 'c': 
       	  res = 1; 
       	  break; 
       	case 'g':
       	  res = 2; 
       	  break; 
       	case 't': 
       	  res = 3; 
       	  break; 
       	default: 
       	  System.out.println("Err: carattere non codificabile!");
       	  System.exit(0); 
       }
       return res; 
	}//[m]f
}//{c}ApproximateEditDistanceBarYossef