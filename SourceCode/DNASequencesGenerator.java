import java.io.*; 

public class DNASequencesGenerator 
{
  public static void main(String[] args) 
  {
    //Lunghezza desiderata per le stringhe 
    int strLength = Integer.parseInt(args[0]); 
    //Numero di stringhe che si desiderano andare a generare con lunghezza pari a strLength 
    int numStrings = Integer.parseInt(args[1]); 
    PrintWriter pw = null; 

    
    try
    {
      pw = new PrintWriter(args[2]); 
    }
    catch(FileNotFoundException fnfe)
    {
    	fnfe.printStackTrace(); 
    }
    
    //pw = new PrintWriter(System.out);
    
    for(int k = 0; k < numStrings; k++)
    {
      for(int i = 0; i < strLength; i++)
      {
        int res = randVal(0,3); 

        switch(res)
        {
         case 0: 
          pw.print("a"); 
          break; 
         case 1: 
          pw.print("c"); 
          break; 
         case 2: 
          pw.print("g");
          break;  
         case 3: 
          pw.print("t"); 
          break; 
        }
      }
      pw.println(); 
      pw.flush();
    }
    
     
  }//[m]main


  public static int randVal(int minVal, int maxVal)
  { 
    int diff = maxVal-minVal;
    return (int)(Math.random()*((double)diff+1.0d))+minVal;
  }//[m]randVal
}//{c}DNASequencesGenerator