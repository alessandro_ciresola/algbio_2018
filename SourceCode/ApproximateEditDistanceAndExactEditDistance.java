import java.util.Scanner;
import java.util.ArrayList;
import java.io.*; 

/**
* Questa classe calcola le seguenti misure: 
* 1)Calcolo della q-gram distance Dq(x,y) in tempo pari a O(|x|+|y|) (paper di Ukkonen) 
* 2)Approssimazione della edit distance tra due stringhe x e y tramite metodo di Sokolov in tempo 
* O((n-w+1)(q2-q1)(2w)) = O((n-w+1) * w) = O(n) dal momento che 1 < w < n 
* 3)Calcolo della edit distance esatta tra due stringhe x e y in tempo O(n*m)
*@version 2.0 
*/

public class ApproximateEditDistanceAndExactEditDistance 
{
	public static final int ALPHABET_SIZE = 4; 

	public static void main(String[] args) 
	{
      //Valori in input necessari per il calcolo della edit distance approssimata tramite il metodo di Sokolov 
      int w = Integer.parseInt(args[0]); 
      int q1 = Integer.parseInt(args[1]);
      int q2 = Integer.parseInt(args[2]);
      
      //Tramite la redirezione di input si prende in ingresso il file con le stringhe di dna da processare 
      Scanner sc = new Scanner(System.in); 
      ArrayList<String> dnaSequences = new ArrayList<String>(); 
      PrintWriter out = null; 

      try
      {
        //args[3] contiene il nome del file su cui si desidera scrivere l'output 
        out = new PrintWriter(args[3]);
      }
      catch(FileNotFoundException fnfe)
      {
        fnfe.printStackTrace(); 
      }

      while(sc.hasNextLine())
      {
      	dnaSequences.add(sc.nextLine()); 
      }

      String s1 = null; 
      String s2 = null; 

      out.println("SKD " + "SKT " + "EXD " + "EXT " + "DEL " + "LOW " + "UPP");

      for(int i = 0; i < dnaSequences.size()-1; i++)
      {
      	for(int j = (i+1); j < dnaSequences.size(); j++)
      	{
          s1 = dnaSequences.get(i); 
          s2 = dnaSequences.get(j); 
          long oldTime = System.nanoTime(); 
          long distance = editDistance(s1, s2, w, q1, q2);
          long newTime = System.nanoTime();  
          long computingTimeRequired = newTime - oldTime; 
          //Stringa in output del metodo di approssimazione di Sokolov 
          //System.out.println(distance + " " + computingTimeRequired);
          
          String xi = s1; 
          String yi = s2;  
        
          int m = xi.length(); 
          int n = yi.length();
        
          char[] x = new char[m+1]; //xi = 0...........m 
          char[] y = new char[n+1]; //yi= 0............n

          for(int h = 0; h < m; h++) 
          { 
            x[h+1] = xi.charAt(h); 
          }
    
          for(int k = 0; k < n; k++) 
          {  
            y[k+1] = yi.charAt(k); 
          }  

          long oldTime2 = System.nanoTime(); 
          int[][] commSubLen = LCS(x, y, m, n); 
          int commLength = commSubLen[m][n]; 
          // ED(x,y) = |x|+|y|-2*|LCS(x,y)| 
          int editDistance = n + m - 2*(commLength);
          long newTime2 = System.nanoTime(); 
          long timeRequired = newTime2 - oldTime2;

          //Calcolo il lower bound e l'upper bound per il metodo di Sokolov 
          long lowerBound = Math.round(((double)(2*(editDistance-5)))/n); 
          long upperBound = Math.round(((double)(2*editDistance*(n+2)))/n); 
          //sokolov_distance | time_sokolov | exactEditDistance | time_for_exact_edit | delta | lowerBound | upperBound
          out.println(distance + " " + computingTimeRequired + " " + editDistance + " " + timeRequired + " " + Math.abs(editDistance-distance) + " " + lowerBound + " " + upperBound); 
      	}//endfor
      }//endfor
      out.flush(); 
	}//[m]main 

    public static long editDistance(String x, String y, int w, int q1, int q2)
    {
       int n = x.length(); 
       int m = y.length(); 

       if(n != m)
       {
       	 System.out.println("L'approssimazione della Edit Distance non e' calcolabile in quanto le stringhe fornite in input hanno lunghezza differente!"); 
       	 System.exit(1); 
       }

       //Denominatore della formula (5) del paper di Sokolov 
       int den = (n - w + 1)*(q2 - q1 + 1); 
       double totalSum = 0; 

       for(int j = 0; j <= n-w; j++)
       {
         totalSum = totalSum + epsDistance(x.substring(j,j+w), y.substring(j,j+w), q1, q2);  
       }

       double approximateDistance = totalSum / den;
       long roundedResult = Math.round(approximateDistance); 

       return roundedResult; 
    }//[m]editDistance

    public static double epsDistance(String x, String y, int q1, int q2)
    {
       double partialSum = 0; 

       for(int q = q1; q <= q2; q++)
       {
       	 partialSum = partialSum + qGramDistance(x, y, q); 
       }

       return partialSum; 
    }//[m]epsDistance


	/**
  *@version 2.0 Riduco il tempo per il calcolo della q-gram distance tra due stringhe sfruttando il metodo 
  * descritto nel paper di Ukkonen 
  */
  public static int qGramDistance(String x, String y, int q)
	{
       if(x.length() != y.length())
       {
       	 System.out.println("Errore: Le stringhe fornite in input non sono della stessa lunghezza!"); 
       	 System.exit(0); 
       } 

       int numbComb = (int)(Math.pow((double)ALPHABET_SIZE, (double)q)); 
       int[] resVec = new int[numbComb];  
       int n = x.length(); 
       int specialCharacter = (n-q+2); 
       //Inizializzo il vettore contenente tutte le possibili combinazioni con tutti n-q+2 (carattere speciale) 
       //in quanto al massimo si hanno n-q+1 occorrenze dello stesso q-gramma 
       for(int i = 0; i < resVec.length; i++)
       {
       	 resVec[i] = specialCharacter; 
       }  

       //Inizializzo la lista di indici che devo mantenere per calcolare in modo efficiente la q-gram distance 
       ArrayList<Integer> indexList = new ArrayList<Integer>(); 

       //Considero le sottostringhe di x e di y del tipo x[i:i+q-1] e y[i:i+q-1] con i=0...n-q = 0...(x.length()-q)
       int prevRank = 0; //Mantengo il rank del q-gramma precedente calcolato 
       char prevChar = 0; //Mantengo il carattere più significativo del q-gramma precedentemente calcolato 
       
       //Lavoro sulla prima stringa x in ingresso 
       for(int i = 0; i <= (n-q); i++)
       {
       	  String q_gram = x.substring(i,i+q); 
          int rk;  

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);     
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
          }
           
          if(resVec[rk] == specialCharacter) //Non e' mai stato letto e devo inserire l'indice nella lista indexList 
          {
            //Ho trovato un'occorrenza del q-gramma codificato in rk 
            resVec[rk] = 1; 
            indexList.add(Integer.valueOf(rk)); 
          }
          else
          {
            //Il q-gramma era gia' stato letto in precedenza per cui l'indice e' gia' stato salvato nella indexList 
            resVec[rk]++; 
          }

          prevRank = rk; 
          prevChar = q_gram.charAt(0);
       }

       //Lavoro sulla seconda stringa y in ingresso 
       prevRank = 0; 
       prevChar = 0; 
       for(int i = 0; i <= n-q; i++)
       {
       	  String q_gram = y.substring(i,i+q); 
          int rk; 

          if(prevRank == 0) //Mi calcolo il rank completo del q-gramma... 
          {
            rk = rank(q_gram);     
          }
          else //Sfrutto il rank del q-gramma precedente 
          {
            rk = (int)((prevRank-(f(prevChar)*Math.pow((double)ALPHABET_SIZE, (double)(q-1))))*ALPHABET_SIZE) + f(q_gram.charAt(q_gram.length()-1));
          }
          
          if(resVec[rk] == specialCharacter) //Non e' mai stato letto e devo inserire l'indice nella lista indexList 
          {
            //Ho trovato un'occorrenza del q-gramma codificato in rk 
            resVec[rk] = -1;  
            indexList.add(Integer.valueOf(rk)); 
          }
          else
          {
            //Il q-gramma era gia' stato letto in precedenza per cui l'indice e' gia' stato salvato nella indexList 
            resVec[rk]--; 
          }

          prevRank = rk; 
          prevChar = q_gram.charAt(0);
       }

       //Eseguendo la somma delle componenti in valore assoluto del vettore resVec con indici nella lista indexList
       //si ottiene la q-gram distance tra le due stringhe x e y  
       int sumResVec = 0; 
       
       for(Integer idx : indexList)
       {
         int intIndex = idx.intValue(); 
         sumResVec = sumResVec + Math.abs(resVec[intIndex]); 
       }

       return sumResVec; 
	}//[m]qGramDistance

	public static int rank(String s)
	{
	  int q_gram_size = s.length(); 
	  int sum = 0; 

	  for(int i = 0; i < q_gram_size; i++)
	  {
        sum = sum + (f(s.charAt(i)) * (int)(Math.pow((double)ALPHABET_SIZE, (double)(q_gram_size-1-i))));  
	  }

    return sum; 
	}//[m]rank

	public static int f(char ch)
	{
       char chL = Character.toLowerCase(ch);  
       int res = -1; 

       switch(chL)
       {
       	case 'a': 
       	  res = 0; 
       	  break; 
       	case 'c': 
       	  res = 1; 
       	  break; 
       	case 'g':
       	  res = 2; 
       	  break; 
       	case 't': 
       	  res = 3; 
       	  break; 
       	default: 
       	  System.out.println("Err: carattere non codificabile!");
       	  System.exit(0); 
       }
       return res; 
	}//[m]f

  //Per il calcolo della lunghezza della LCS 
  public static int[][] LCS(char[] x, char[] y, int m, int n) 
  { 
    int[][] L = new int[m+1][n+1];
    int i; 
    int j; 

    for(i=0; i<m+1; i++) 
       L[i][0] = 0; 
   
    for(j=1; j<n+1; j++) 
     L[0][j] = 0; 

    for(i=1; i<m+1; i++) 
    { 
      for(j=1; j<n+1; j++) 
      { 
        if(x[i]==y[j]) 
        { 
          L[i][j] = L[i-1][j-1] + 1; 
        } 
        else 
        { 
          if( L[i-1][j] >= L[i][j-1] ) 
          { 
            L[i][j] = L[i-1][j] ; 
          } 
          else 
          { 
            L[i][j] = L[i][j-1] ; 
          } 
        } 
      } 
   } 
   return L; 
  }//[m]LCS
}//{c}AlgBioProject
