
/**
*Classe che mantiene le coppie (q_gram, blockNumber) richieste per l'algoritmo di Bar-Yossef 
*@version 1.0 
*/

public class BarTuple 
{
	private String qGram; 
 	private int blockNumber; 

	public BarTuple(String qGram, int blockNumber)
	{
	  this.qGram = qGram; 
	  this.blockNumber = blockNumber; 
	}
    
    public int getBlockNum()
    {
      return blockNumber; 
    }

    public String getQGram()
    {
      return qGram; 
    }
}//{c}BarTuple